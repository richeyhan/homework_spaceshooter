﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {

    public float speed;
    public float tilt;
    public GameObject shot;
    public Transform shotSpwan;
    public Boundary boundary;

    private float nextFire;
    public float fireRate;

    void Awake()
    {
        
        
    }
    
    // Use this for initialization
    void Start()
    {
    }
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        Rigidbody rb;
        rb = GetComponent<Rigidbody>();
        rb.velocity = movement * speed;                                 //rb.velocity메서드 적용 시 해당 vector값으로 움직이기 시작한다!
        rb.position = new Vector3                                       //rb.position을 정의하고, x/z좌표를 Mathf.Clamp로 최대/최소값 제한한다.
            (
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)
            );
        rb.rotation = Quaternion.Euler(0f, 0f, rb.velocity.x * -tilt);  //Euler는 각 축을 기준으로 회전하는 값을 리턴해준다...
    }
   
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpwan.position, shotSpwan.rotation);
            AudioSource playerWeapon = GetComponent<AudioSource>();
            playerWeapon.Play();

        }

    }
}
